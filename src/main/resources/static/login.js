'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;


function connect(event) {
    username = document.querySelector('#name').value.trim();

    if (username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws-endpoint');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected);
    }
    event.preventDefault();
}


function onConnected() {
    // Subscribe to the Public Topic
    //  stompClient.subscribe('/chat/users', onMessageReceived);

    stompClient.subscribe('/chat/users', function (payload) {
        // onMessageReceived(JSON.parse(message.body).text);
        // onMessageReceived(payload);
        onMessageReceived(JSON.parse(payload.body));
    });
    // Tell your username to the server
    stompClient.send("/app/chat.login",
        {},
        JSON.stringify(
            {
                id: "",
                username: username,
                password: ""
            })
    )

    connectingElement.classList.add('hidden');
}


function send(event) {
    var messageContent = messageInput.value.trim();

    if (messageContent && stompClient) {
        var user = {
            sender: username,
            content: messageInput.value,
        };

        stompClient.send("/app/chat.user", {}, JSON.stringify(user));
        messageInput.value = '';
    }
    event.preventDefault();
}

function onMessageReceived(payload) {

    console.log(payload)


    let index = 0;
    while(index < payload.length){
        console.log(payload[index].username);
        var theDiv = document.getElementById("messageArea");
        var messageElement = document.createElement('li');

        var createA = document.createElement('a');
        var createAText = document.createTextNode("Send");
        createA.setAttribute('href', "/chat/"+payload[index].id);
        createA.appendChild(createAText);

        var content = document.createTextNode(payload[index].username);
        messageElement.appendChild(content);
        messageElement.appendChild(createA);
        theDiv.appendChild(messageElement);
        index += 1;

    }




}


usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', send, true)