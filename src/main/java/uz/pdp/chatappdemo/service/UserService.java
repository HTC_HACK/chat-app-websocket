package uz.pdp.chatappdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.chatappdemo.entity.Message;
import uz.pdp.chatappdemo.entity.User;
import uz.pdp.chatappdemo.repo.MessageRepository;
import uz.pdp.chatappdemo.repo.UserRepository;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    MessageRepository messageRepository;

    public void saveUser(User user) {
        userRepository.save(user);
    }

    public List<User> getAllUsers() {

        return userRepository.findAll();
    }

    public User findByUsername(String username) {
        if (userRepository.findByUsername(username).isPresent()) {
            return userRepository.findByUsername(username).get();
        }
        return null;
    }

    public List<Message> getAllMessage(String username)
    {
        User user = userRepository.findByUsername(username).get();
        return messageRepository.findBySender(user);
    }
}
