package uz.pdp.chatappdemo.controller;


//Asatbek Xalimojnov 4/29/22 4:16 PM


import nonapi.io.github.classgraph.json.JSONSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import uz.pdp.chatappdemo.entity.Message;
import uz.pdp.chatappdemo.entity.User;
import uz.pdp.chatappdemo.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {


    @Autowired
    UserService userService;

    @Autowired
    SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/chat.login")
    @SendTo("/chat/users")
    public List<User> login(@RequestBody User user, SimpMessageHeaderAccessor headerAccessor) throws Exception {
        headerAccessor.getSessionAttributes().put("username",user.getUsername());
        return userService.getAllUsers();
    }

    @MessageMapping("/chat.user")
    @SendTo("/chat/users")
    public List<User> userList() {

        return userService.getAllUsers();
    }

    @MessageMapping("/chat.room")
//    @SendTo("/chat/messages")
    public void getMessage(@RequestBody User user,@RequestBody User receiver, SimpMessageHeaderAccessor headerAccessor) throws Exception {
        headerAccessor.getSessionAttributes().put("username",user.getUsername());

        messagingTemplate.convertAndSendToUser(
                receiver.getUsername(),
                "/chat/",
                userService.getAllMessage(user.getUsername()));

    }
}
