package uz.pdp.chatappdemo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.chatappdemo.entity.Message;
import uz.pdp.chatappdemo.entity.User;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message,Long> {

    List<Message> findBySender(User user);
}
